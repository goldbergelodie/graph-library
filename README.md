# Graph Library :bar_chart: :books:

## The project

Graph Library is a graphic representation of my personal library using [Graphviz](https://graphviz.org/) and rendered in a web page in SVG format. It aims to render a graph as a visual proposition of my reads and references (books, essays, conferences, videos...). Works and authors/contributors are put in relation to one another. The graph is semi-controlled on the form it takes and mainly depends on the [layout engine](https://graphviz.org/docs/layouts/) chosen.

### Graphviz

>Graphviz is open source graph visualization software. It has several main graph layout programs.(...)The Graphviz layout programs take descriptions of graphs in a simple text language, and make diagrams in several useful formats such as images and SVG for web pages, Postscript for inclusion in PDF or other documents; or display in an interactive graph browser. (Graphviz also supports GXL, an XML dialect.) (https://graphviz.org/about/)

### Rendering

Use of [GraPHP library](https://github.com/graphp/graphviz). 

#### Install 

[Composer](https://getcomposer.org/doc/00-intro.md) is recommended.

`$ composer require graphp/graphviz`

Install GraphViz

`$ sudo apt install graphviz`

## Current Library :art:


Constellations are drawn from people (authors/contributors) and their work by paths.

## Development :pencil2:

### "Library" section
The "library" section is under construction. It presents the library graph in a list - in more formal and readable data.

### JSON output
Graphviz data is rendered in a json file with the command line `dot -Tdot_json file.dot -o output.json`



:goat: :goat: :goat: :goat: :goat:
