# TO DO :punch: :punch: :punch:

## Priorities
- Make items on the graph not overlap 
- Loop through items and edges and connect them with corresponding IDs
- Add content in each item (bio, description, language)
- reorganize project

## Optional
- Create search bar for graph and list
- Create zoom button for the graph
