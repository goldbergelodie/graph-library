<?php
    $meta = $item['meta'];

        $userName = $meta['createdByUser']['username'];
        $userLabel = $meta['createdByUser']['name'];
        $creatorSummary = $meta['creatorSummary'];
        $parsedDate = $meta['parsedDate'];
   
    $data = $item['data'];

        $key = $data['key'];
        $itemType = $data['itemType'];
        $title = $data['title'];

        $creators = $data['creators'];
        foreach($creators as $creator){
            $creatorType = $creator['creatorType'];
        }

        $abstractNote = $data['abstractNote'];
        $series = $data['series'];
        $edition = $data['edition'];
        $place = $data['place'];
        $publisher = $data['publisher'];
        $date = $data['date'];
        $language = $data['language'];
        $isbn = $data['ISBN'];
        $shortTitle = $data['shortTitle'];
        $link = $data['url'];
        $note = $data['note'];
        $tags = $data['tags'];
        
        $attachement = $data['attachement'];
        $attachementUrl = $data['attachement']['url'];

?>
