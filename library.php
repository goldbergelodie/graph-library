<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Graphs Research War Zone</title>
    <link rel="stylesheet" href="./css/styles.css">
    
</head>
<body>
    <?php  
    $url = file_get_contents('https://api.zotero.org/groups/4543219/items?limit=1000');
    $array = json_decode($url, TRUE);
    $items = array_merge($array);

    //var_dump($items);
    ?>

    <div class="list">
        <ul>
            <?php
            include 'includes/functions.php';

            foreach($items as $item){
                
                include 'includes/variables.php';

                echo '<li>'.'<a class="link" href="' . $link .'"target="_blank">'.  ($shortTitle ? $shortTitle : $title) .' '.'</a>'.'<br>';
                echo nameConstructor($creators).'<br>'; 
                // echo '<div class="abstractNote">'.$abstractNote.'</div>'.'</li>';
                // echo $creatorType;
                echo tags($tags);
            };   
            ?>
        </ul>
    </div>
    <div class="menu-checkbox">
        <h2><input type="checkbox" checked name="GD" value="GD">&nbsp;&nbsp;graphic & web design</h2>

    </div>
</body>
</html>